
<!-- README.md is generated from README.Rmd. Please edit that file -->
chifishr
========

The goal of `chifishr` is to provide a helper function for calculating a p-value from either a Chi-squared or a Fisher Exact test, depending on if a warning is thrown from the Chi-squared test due to small expected counts leading to poor p-value approximations.

Installation
------------

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/scheidec/chifishr")
```

Example
-------

If we are comparing two categorical variables and one has a category with small counts, a warning is thrown from the `chisq.test` function, and the `fisher.test` is preferred. The `chi_fisher_p` function will return the p-value from `fisher.test` if warnings are present, or the p-value from `chisq.test` otherwise.

``` r
# warning is present, Fisher p-value is returned
chi_fisher_p(treatment, "outcome", "treatment")

# no warning is present, Chi-squared p-value returned
chi_fisher_p(treatment, "gender", "treatment")
```
